# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.3](https://gitlab.com/felipecesr1/monorepo/compare/p1@1.1.2...p1@1.1.3) (2023-01-02)

**Note:** Version bump only for package p1





## [1.1.2](https://gitlab.com/felipecesr1/monorepo/compare/p1@1.1.1...p1@1.1.2) (2023-01-02)

**Note:** Version bump only for package p1





## [1.1.1](https://gitlab.com/felipecesr1/monorepo/compare/p1@1.1.0...p1@1.1.1) (2023-01-02)

**Note:** Version bump only for package p1





# [1.1.0](https://gitlab.com/felipecesr1/monorepo/compare/p1@1.0.2...p1@1.1.0) (2023-01-02)


### Features

* p1 ([4c074a2](https://gitlab.com/felipecesr1/monorepo/commit/4c074a201e36a5548ad114095f3799e7136ea4f8))
